//
//  PostCell.swift
//  Imgur iOS
//
//  Created by rightmeow on 9/21/17.
//  Copyright © 2017 Duckensburg. All rights reserved.
//

import UIKit

class PostCell: UICollectionViewCell {

    var post: Post? {
        didSet {
            updateCell()
        }
    }

    static let cell_id = String(describing: PostCell.self)

    @IBOutlet weak var frameView: UIView!
    @IBOutlet weak var postImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!

    private func updateCell() {
        
    }

    private func setupCell() {
        // frameView
        frameView.layer.cornerRadius = 5
        frameView.backgroundColor = UIColor.black
        frameView.contentMode = UIViewContentMode.scaleAspectFill
        frameView.layer.borderColor = UIColor.orange.cgColor
        frameView.layer.borderWidth = 2
        // postImageView
        postImageView.contentMode = UIViewContentMode.scaleAspectFill
        postImageView.parallax(magnitude: 10)
    }
    
}
