//
//  WebServiceManager.swift
//  Imgur
//
//  Created by rightmeow on 9/2/17.
//  Copyright © 2017 Duckensburg. All rights reserved.
//

import Foundation

protocol WebServiceDelegate: NSObjectProtocol {
    // error
    func webService(_ manager: WebServiceManager, didErr error: Error)
    // auth
    func webServiceDidLogin(_ manager: WebServiceManager, user: Any, session: Any)
    func webServiceDidLogout(_ manager: WebServiceManager)
    func webServiceDidSignup(_ manager: WebServiceManager)
    // posts
    func webSerivceDidCreate(_ manager: WebServiceManager, posts: Any)
    func webServiceDidFetch(_ manager: WebServiceManager, posts: Any)
    func webServiceDidUpdate(_ manager: WebServiceManager, posts: Any)
    func webServiceDidDelete(_ manager: WebServiceManager, posts: Any)
    // comments
    func webServiceDidCreate(_ manager: WebServiceManager, comments: Any)
    func webServiceDidFetch(_ manager: WebServiceManager, comments: Any)
    func webServiceDidUpdate(_ manager: WebServiceManager, comments: Any)
    func webServiceDidDelete(_ manager: WebServiceManager, comments: Any)
}

extension WebServiceDelegate {
    // auth
    func webServiceDidLogin(_ manager: WebServiceManager, user: Any, session: Any) {}
    func webServiceDidLogout(_ manager: WebServiceManager) {}
    func webServiceDidSignup(_ manager: WebServiceManager) {}
    // posts
    func webSerivceDidCreate(_ manager: WebServiceManager, posts: Any) {}
    func webServiceDidFetch(_ manager: WebServiceManager, posts: Any) {}
    func webServiceDidUpdate(_ manager: WebServiceManager, posts: Any) {}
    func webServiceDidDelete(_ manager: WebServiceManager, posts: Any) {}
    // comments
    func webServiceDidCreate(_ manager: WebServiceManager, comments: Any) {}
    func webServiceDidFetch(_ manager: WebServiceManager, comments: Any) {}
    func webServiceDidUpdate(_ manager: WebServiceManager, comments: Any) {}
    func webServiceDidDelete(_ manager: WebServiceManager, comments: Any) {}
}

class WebServiceManager: NSObject {

    weak var delegate: WebServiceDelegate?

}

struct WebServiceConfigurations {

    static let baseUrl = "https://api.imgur.com/"
    static let clientId = "7ac1b41174d76e7"
    static let clientSecret = "831367cad5e7336f645437f52cfb0a26112e3423"

}
